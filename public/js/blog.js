$(function () {
    $("div.blog-item-body").on("mouseenter mouseleave", function () {
        $(this).toggleClass("blog-item-pointed");
    });
});

$(window).on("scroll", function () {
    let isScrolledDown = $(this).scrollTop() > 100;

    $('nav.navbar').css("padding", isScrolledDown ? "25px 40px 25px 40px" : "40px");
});

$(window)
    .off("load", function () {
        $("body").html('<p>Loading...</p>');
    })
    .on("load", function () {
        $('.blog-list-item').fadeIn();
        $('.blog-thumbnail').fadeIn();
    });
